import React, { useContext } from "react";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Box from "@material-ui/core/Box";

import categories from "../utils/categories";
import useStyles from "../style";
import Categories from "../utils/categories.in";
import { Context } from "../hooks/context";

export default function ToggleButtonsMultiple() {
  const [formats, setFormats] = React.useState<Categories[]>([]);
  const classes = useStyles();
  const { dispatch } = useContext(Context);

  const handleFormat = (
    event: React.MouseEvent<HTMLElement>,
    newFormats: Categories[]
  ) => {
    setFormats(newFormats);
    const value = newFormats.reduce((acc, el) => acc + el.points, 0);
    dispatch({ type: "SET_VALUE", payload: value });
  };

  const renderCategories = () =>
    categories.map((category) => (
      <ToggleButton key={category.name} value={category}>
        {category.label}
      </ToggleButton>
    ));

  return (
    <Box display="flex" justifyContent="center">
      <ToggleButtonGroup
        className={classes.root}
        value={formats}
        onChange={handleFormat}
        aria-label="text formatting"
      >
        {renderCategories()}
      </ToggleButtonGroup>
    </Box>
  );
}
