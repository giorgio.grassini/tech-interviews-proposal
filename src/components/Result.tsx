import React, { useContext } from "react";
import Box from "@material-ui/core/Box";

import { Context, Ctx } from "../hooks/context";

export default function Result() {
  const { state } = useContext<Ctx>(Context);

  return (
    <Box display="flex" justifyContent="center">
      <h1>Result {state.value}</h1>
    </Box>
  );
}
