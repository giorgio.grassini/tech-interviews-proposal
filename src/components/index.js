import Documentation from "./Documentation";
import Form from "./Form";
import Result from "./Result";

export { Documentation, Form, Result };
