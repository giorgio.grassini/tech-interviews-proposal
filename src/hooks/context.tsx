import React, { createContext, useReducer } from "react";

interface State {
  value: number;
}
export interface Ctx {
  state: State;
  dispatch: React.Dispatch<Action>;
}

type Action = { type: "SET_VALUE"; payload: number };

const Reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "SET_VALUE":
      return {
        ...state,
        value: action.payload,
      };
    default:
      return state;
  }
};

const initialState = {
  state: { value: 0 },
  dispatch: () => {},
};

export const Context = createContext<Ctx>(initialState);

interface Props {
  children: JSX.Element[] | JSX.Element;
}

const Store = ({ children }: Props) => {
  const [state, dispatch] = useReducer(Reducer, initialState.state);
  return (
    <Context.Provider value={{ state, dispatch }}>
      {children}
    </Context.Provider>
  );
};

export default Store;
