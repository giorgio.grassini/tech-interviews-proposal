import React from "react";

import * as Components from "../components";

export default function Home() {
  return (
    <React.Fragment>
      <Components.Documentation />
      <Components.Form />
      <Components.Result />
    </React.Fragment>
  );
}
