import {
  makeStyles,
  createStyles,
  Theme,
} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "50%",
      flexWrap: "wrap",
      justifyContent: "center",
      "& > .MuiButtonBase-root": {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        backgroundColor: "#efefef",
        borderRadius: "18px !important",
        color: "#6b6b6b",
        border: "0.1px solid black",
      },
      "& > .Mui-selected": {
        marginLeft: `${theme.spacing(1)}px !important`,
        borderLeft: "0.1px solid black !important",
        padding: theme.spacing(1),
        color: "#fff",
        backgroundColor: "rgb(0, 85, 204) !important",
      },
    },
  })
);

export default useStyles;
