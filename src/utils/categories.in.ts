export default interface Categories {
  name: string;
  label: string;
  points: number;
}
