import Categories from "./categories.in";

const categories: Categories[] = [
  { name: "architecture", label: "Architecture", points: 1 },
  { name: "cleanCode", label: "Clean Code", points: 1 },
  { name: "styledComponent", label: "Styled-component", points: 1 },
  {
    name: "conventionalCommit",
    label: "Conventional Commit",
    points: 1,
  },
  { name: "testCoverage", label: "Test Coverage", points: 1 },
  { name: "sass", label: "sass", points: 1 },
  { name: "performance", label: "Performance", points: 1 },
  { name: "deployed", label: "Deployed", points: 1 },
  { name: "typescript", label: "Typescript", points: 1 },
  { name: "eslint", label: "Eslint", points: 1 },
  { name: "reactContextAPI", label: "React Context API", points: 1 },
];

export default categories;
